# Multiple Contact Sheet QT

This program executes the command specified for each input file or each file found in folders, is designed to work together with [subtitles_contact_sheet](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt), but can run any other program. It can run as a daemon searching for new files in the folders periodically.

## Usage

Accepts one or many files or directories as a parameter, files are processed directly regardless of their type, directories need the -r and -f parameters to enter them and select the file types

### Examples

Run subtitles_contact_sheet for all mp4 videos inside a folder

    multiple_contact_sheet folder/*.mp4

Same as above but recursive, search files inside subforders too

    multiple_contact_sheet folder/ -r -f "*.mp4"

Send params to subtitles_contact_sheet

    multiple_contact_sheet folder/ -r -f "*.mp4" -- --no_overwrite

Same as above but with 4 simultaneos process

    multiple_contact_sheet folder/ -r -f "*.mp4" -p 4 -- --no_overwrite

Using another program example ffmpeg

    multiple_contact_sheet folder/ -r -f "*.mp4" -c ffmpeg -- -y -i %file% other_folder/%filename%.avi

Scan folder every 60 seconds for new video files and wait 10 seconds after the modification date

    multiple_contact_sheet folder/ -r --video -t 60 -m 10 -- --no_overwrite

Uses the file system events to detect changes

    multiple_contact_sheet folder/ -r --video -t 60 -w -- --no_overwrite

### Parameters

- **-r**: Recursive through the specified directories
- **-f, --file <tile_types>**: Select this files only, example "*.mkv". It is possible to put as many -f as required
- **-t, --time <seconds>**: Searches for new files every <seconds>. This means that the program never ends by itself
- **-w, --watch**: Uses file system events to detect when there are changes inside the folders, It is recommended to always use this option together with --time, not all platforms have good file system events
- **-m, --modified <seconds>**: Only select files with modification time older than this. This option avoids processing files that are currently being written, Default for windows: 10, on linux 1
- **--recheck <seconds>**: Recheck incomplete files after this time. 0 to disable. Default for windows: 10, on linux 0
- **-d, --delete <seconds>**: Time in seconds when it performs a check of the deleted files to remove them from its internal list, default 3600
- **-q, --quiet**: Hide all messages, except for errors
- **--video**: Same as -f "\*.mkv" -f "\*.mp4" -f "\*.avi" -f "\*.mov" -f "\*.wmv" -f "\*.m4v"
- **--subs**: Same as -f "\*.srt" -f "\*.vtt" -f "\*.ass"
- **-p, --parallel <1-16>**: Launch this number of processes in parallel
- **--cmd**: Show commands without executing them
- **--no_fail**: Continue even if a process fail
- **-c, --command <program>**: Program to launch
- **--**: Separator, the following parameters are passed to the program to be executed

### Parameter Substitution

- **%file%:**: Defines te position of file in the params
- **%filename%:**: Input file name, without path and extension
- **%path%:**: The path to the input file
- **%ext%:**: The extension of the input file

## Compiling

### Requirements

Needs qmake and QT base development files, In debian they are the packages *qtbase5-dev* and *qt5-qmake* or *qmake6* and *qt6-base-dev* for the QT6 version.

### Linux

To compile it is the same as subtitles_contact_sheet. You can follow the instructions by changing the paths and names to the ones in this repository

[GNU/Linux Compilations instructions](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/blob/main/compile_linux.md)

### Windows

The easiest way is to download the [QT installer](https://www.qt.io/download-qt-installer) and install **QT Creator** and a version of Qt, for example *"Qt 5.15.2 MiniGw 8.1.0 32-bit"*

With this it would be enough to make it work, but in windows the most convenient is to [compile it statically](https://wiki.qt.io/Building_a_static_Qt_for_Windows_using_MinGW)
