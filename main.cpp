#include <QCoreApplication>
#include <stdio.h>
#include <stdlib.h>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QProcess>
#include <QTime>
#include <QThread>
#include <QFileSystemWatcher>

const char *help =
#include "help.txt"
			;

bool Quiet = false;
bool Daemon = false;

void PrintInfo(QString line) {
	if (!Quiet) {
		fprintf(stdout, "%s\n", line.toUtf8().data());
		fflush(stdout);
	}
}

void PrintError(QString line) {
	fprintf(stderr, "Error: %s\n", line.toUtf8().data());
	fflush(stderr);
}

void PrintCommand(QString command) {
	fprintf(stdout, "%s\n", command.toUtf8().data());
	fflush(stdout);
}

QStringList StartDirList;
QStringList DirListQueue;
QStringList FileListQueue;
QStringList FileListDone;
QStringList FileListProcessing;
QStringList FileListIncomplete;
QStringList FileFilters;

int ScanDirsEvery = 0;
int CleanDeletedFilesEvery = 3600;

#ifdef Q_OS_WIN
	//On windows the watcher not works correctly, this value mitigated the problems
	int ModifiedTime = 10;
	int CheckIncompleteEvery = 10;
#else
	//On linux this option isn't necesary
	int ModifiedTime = 1;
	int CheckIncompleteEvery = 0;
#endif

bool Recursive = false;
bool OwnParams = true;
bool NoFail = false;
int Watch = false;
bool CMD = false;

QFileSystemWatcher *Watcher;

void DirSelect(const QString Dir) {
	QDir DirObj(Dir);

	if (!DirObj.exists()) return;

	QStringList watched_dirs = Watcher->directories();
	if (Watch && !watched_dirs.contains(Dir)) {
		PrintInfo("Add Watch: " + Dir);
		Watcher->addPath(Dir);
	}

	QStringList CurrentList = DirObj.entryList(FileFilters, QDir::Files);
	for (int i = 0; i < CurrentList.count(); ++i) {
		QString filename = QDir::toNativeSeparators(DirObj.absolutePath() + QDir::separator() + CurrentList.at(i));
		if (FileListDone.contains(filename) ||
			FileListQueue.contains(filename) ||
			FileListIncomplete.contains(filename) ||
			FileListProcessing.contains(filename)) continue;
		QFileInfo info(filename);
		if (info.size() == 0) {
			PrintInfo("File Ignored size 0: " + filename);
			if (CheckIncompleteEvery > 0) FileListIncomplete.append(filename);
			continue;
		}
		if (ModifiedTime > 0 && info.lastModified().secsTo(QDateTime::currentDateTime()) < ModifiedTime) {
			PrintInfo("File Ignored modification time (" +
					  QString::number(info.lastModified().secsTo(QDateTime::currentDateTime())) + "): " + filename);
			if (CheckIncompleteEvery > 0) FileListIncomplete.append(filename);
			continue;
		}
		FileListQueue.append(filename);
	}
	if (Recursive) {
		CurrentList = DirObj.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
		for (int i = 0; i < CurrentList.count(); ++i) {
			DirListQueue.append(QDir::toNativeSeparators(DirObj.absolutePath() + QDir::separator() + CurrentList.at(i)));
		}
	}
}

struct instance_t {
	QProcess *Process;
	QString File;
	bool InProgress;
	QString Command;
};

int CurrentInstances = 1;
QList<instance_t> Instances;

int main(int argc, char *argv[]) {
	QCoreApplication a(argc, argv);
	Watcher = new QFileSystemWatcher(&a);

	QString Command;
	QStringList ExtParams;

	QTime LastTime = QTime::currentTime();
	QTime LastDeleted = QTime::currentTime();
	QTime LastIncompleteCheck = QTime::currentTime();
	QTime CurrentTime = LastTime;

#define printhelp() fprintf(stdout, help, a.applicationName().toUtf8().constData())

	if (argc < 2) {
		printhelp();
		return 1;
	}

	for (int param_i = 1; param_i < argc; ++param_i) {
		QString Param = argv[param_i];
		if (OwnParams) {
			if (Param == "-r" || Param == "--recursive" ) {
				Recursive = true;

			} else if (Param == "-f" || Param == "--file") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --file or -f");
					return 1;
				}
				FileFilters << argv[param_i];

			} else if (Param == "--video") {
				FileFilters << "*.mkv" << "*.avi" << "*.mp4" << "*.mov" << "*.m4v" << "*.wmv";

			} else if (Param == "--subs") {
				FileFilters << "*.srt" << "*.vtt" << "*.ass";

			} else if (Param == "--no_fail") {
				NoFail = true;

			} else if (Param == "-c" || Param == "--commnad") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --command or -c");
					return 1;
				}
				Command = argv[param_i];
			} else if (Param == "--") {
				OwnParams = false;
			} else if (Param == "-h" || Param == "-?" || Param == "-help" || Param == "--help") {
				printhelp();
				return 0;
			} else if (Param == "-p" || Param == "--parallel") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --parallel or -p");
					return 1;
				}
				CurrentInstances = QString(argv[param_i]).toInt();
			} else if (Param == "-t" || Param == "--time") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --time or -t");
					return 1;
				}
				ScanDirsEvery = QString(argv[param_i]).toInt();
				Daemon = true;

			} else if (Param == "-d" || Param == "--delete") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --delete or -d");
					return 1;
				}
				CleanDeletedFilesEvery = QString(argv[param_i]).toInt();
			} else if (Param == "-w" || Param == "--watch") {
				Watch = true;
				Daemon = true;
			} else if (Param == "-m" || Param == "--modified") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --modified or -m");
					return 1;
				}
				ModifiedTime = QString(argv[param_i]).toInt();
			} else if (Param == "--recheck") {
				param_i++;
				if (param_i == argc) {
					PrintError("Incorrect use of --recheck");
					return 1;
				}
				CheckIncompleteEvery = QString(argv[param_i]).toInt();
			} else if (Param == "--cmd") {
				CMD = true;
			} else if (Param == "-q" || Param == "--quiet") {
				Quiet = true;
			} else {
				QFileInfo CheckInputFile(Param);
				if (CheckInputFile.isDir()) {
					StartDirList.append(Param);
					continue;
				} else if (CheckInputFile.isFile()) {
					FileListQueue.append(QDir::toNativeSeparators(Param));
					continue;
				}
			}

		} else {
			ExtParams.append(Param);
		}
	}

	if (StartDirList.isEmpty() && FileListQueue.isEmpty()) {
		PrintError("No input directories or files");
		printhelp();
		return 1;
	}

	if (Command == "") {
		#ifdef Q_OS_WIN
		QString SCS = a.applicationDirPath() + QDir::separator() + "subtitles_contact_sheet.exe";
		#else
		QString SCS = a.applicationDirPath() + QDir::separator() + "subtitles_contact_sheet";
		#endif

		if (QFile::exists(SCS)) {
			Command = SCS;
		} else {
			PrintError("No command especified");
			printhelp();
			return 1;
		}
	}

	if (CurrentInstances < 1) CurrentInstances = 1;
	if (CurrentInstances > 16) CurrentInstances = 16;
	for (int i = 0; i < CurrentInstances; i++) {
		Instances.append({new QProcess(), "", false, ""});
	}

	if (CMD) {
		Quiet = true;
		CurrentInstances = 1;
	}

	DirListQueue = StartDirList;

	QObject::connect(Watcher, &QFileSystemWatcher::directoryChanged, &a, [ = ](QString Dir) {
		PrintInfo("Dir change: " + Dir);
		QStringList currentwatches = Watcher->directories();
		for (int i = 0; i < currentwatches.count() ; i++) {
			QDir dir(currentwatches.at(i));
			if (dir.exists()) continue;
			Watcher->removePath(currentwatches.at(i));
			PrintInfo("Remove Watch: " + currentwatches.at(i) + " Dir " + Dir);
		}
		if (!currentwatches.contains(Dir)) return;
		DirListQueue.append(Dir);
	});

	bool Running = true;

	while (Running) {

		Running = false;
		for (int i = 0; i < CurrentInstances; i++) {
			instance_t *Instance = &Instances[i];

			if (Instance->Process->state() == QProcess::Running ) {
				Running = true;
				Instance->Process->waitForFinished(20);
			}

			if (Instance->Process->state() == QProcess::NotRunning) {

				if (Instance->InProgress) {
					if (Instance->Process->exitStatus() == QProcess::CrashExit) {
						QString error = Instance->Process->readAllStandardError();
						PrintError("Executing command: " + Instance->Command);
						PrintError(error);
						if (!NoFail) return 1;
					} else {
						if (Instance->Process->exitCode() != 0) {
							QString error = Instance->Process->readAllStandardError();
							PrintError("Executing command: " + Instance->Command);
							PrintError(error);
							if (!NoFail) return 1;
						}
					}
					FileListDone.append(Instance->File);
					FileListProcessing.removeAll(Instance->File);
					Instance->File = "";
					Instance->Command = "";
					Instance->InProgress = false;
				}

				//Select next free instance
				if (!FileListQueue.isEmpty()) {
					Instance->File = FileListQueue.takeFirst();
					QFileInfo FileInfo(Instance->File);
					QString FileName = FileInfo.completeBaseName();
					QString Path = FileInfo.path();
					QString Ext = FileInfo.suffix();

					FileListProcessing.append(Instance->File);
					Instance->InProgress = true;
					Running = true;

					bool FoundFile = false;

					QStringList Params = ExtParams;
					for (int i = 0; i < Params.count(); ++i) {
						QString Param = Params.at(i);
						if (Param.contains("%file%")) {
							FoundFile = true;
							Params.replace(i, Param.replace("%file%", Instance->File));
						}
						if (Param.contains("%filename%")) {
							Params.replace(i, Param.replace("%filename%", FileName));
						}
						if (Param.contains("%path%")) {
							Params.replace(i, Param.replace("%path%", Path));
						}
						if (Param.contains("%ext%")) {
							Params.replace(i, Param.replace("%ext%", Ext));
						}
					}

					if (!FoundFile) Params.prepend(Instance->File);

					Instance->Command = Command;
					for (int i = 0; i < Params.count(); ++i) {
						if (Params.at(i).contains(" ")) {
							Instance->Command.append(" \"" + Params.at(i) + "\"");
						} else {
							Instance->Command.append(" " + Params.at(i));
						}
					}

					PrintInfo("Process " + QString::number(i + 1) + ": " + Instance->Command + "\n");
					if (CMD) {
						PrintCommand(Instance->Command);
					} else {
						Instance->Process->start(Command, Params);
						if (!Instance->Process->waitForStarted(-1)) {
							PrintError("Executing command: " + Instance->Command);
							PrintError(Instance->Process->errorString());
							if (!NoFail) return 1;
						}
					}
				}
			}
		}

		if (Daemon) {
			Running = true;

			if (CleanDeletedFilesEvery > 0) {
				CurrentTime = QTime::currentTime();
				if (LastDeleted.secsTo(CurrentTime) >= CleanDeletedFilesEvery) {
					LastDeleted = CurrentTime;
					for (int i = FileListDone.count() - 1; i >= 0; i--) {
						if (!QFile::exists(FileListDone.at(i))) {
							PrintInfo("Cleaning deleted file: " + FileListDone.at(i));
							FileListDone.removeAt(i);
						}
					}
				}
			}

			if (CheckIncompleteEvery > 0 && FileListIncomplete.count() > 0) {
				CurrentTime = QTime::currentTime();
				if (LastIncompleteCheck.secsTo(CurrentTime) >= CheckIncompleteEvery) {
					LastIncompleteCheck = CurrentTime;
					for (int i = FileListIncomplete.count() - 1; i >= 0; i--) {
						PrintInfo("Rechecking file: " + FileListIncomplete.at(i));
						QFileInfo info(FileListIncomplete.at(i));
						if (!info.exists()) {
							FileListIncomplete.removeAt(i);
							continue;
						}
						if (info.size() == 0) continue;
						if (ModifiedTime > 0 && info.lastModified().secsTo(QDateTime::currentDateTime()) < ModifiedTime) continue;
						FileListQueue.append(FileListIncomplete.at(i));
						FileListIncomplete.removeAt(i);
					}
				}
			}
		}

		if (ScanDirsEvery > 0) {
			CurrentTime = QTime::currentTime();
			if (LastTime.secsTo(CurrentTime) >= ScanDirsEvery) {
				LastTime = CurrentTime;
				DirListQueue.append(StartDirList);
			}
		}

		if (!DirListQueue.isEmpty()) {
			DirSelect(DirListQueue.takeFirst());
			Running = true;
		}

		if (Watch) a.processEvents(QEventLoop::AllEvents, 100);
		QThread::msleep(100);
	}

	for (int i = 0; i < CurrentInstances; i++) {
		delete Instances[i].Process;
	}

	return 0;
}
